package controllers

import (
	"fmt"
	"tugas/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LoginInput struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
    Email    string `json:"email" binding:"required"`
}


func Login(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input LoginInput

    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c, "error", c.Errors.Errors())
        return
    }

    u := models.User{}
    u.Username = input.Username
    u.Password = input.Password

    token, err := models.LoginCheck(u.Username, u.Password, db)
    if err != nil {
        fmt.Println(err)
        SendError(c, "username or password is incorrect.", c.Errors.Errors())
        return
    }

    // Fetch the user from the database to get the email
    if err := db.Where("username = ?", u.Username).First(&u).Error; err != nil {
        SendError(c, "failed to fetch user details.", c.Errors.Errors())
        return
    }

    user := map[string]string{
        "username": u.Username,
        "email":    u.Email,
    }

    // Send the user details along with the token in the response
    response := gin.H{
        "user":  user,
        "token": token,
    }
	SendResponse(c, response, "login success")

}


func Register(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input RegisterInput

    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c, "error", c.Errors.Errors())
        return
    }

    // Check if email already exists in the database
    var existingUser models.User
    if err := db.Where("email = ?", input.Email).First(&existingUser).Error; err == nil {
        SendError(c, "Email already registered", nil)
        return
    }

    u := models.User{}
    u.Username = input.Username
    u.Email = input.Email
    u.Password = input.Password

    _, err := u.SaveUser(db)

    if err != nil {
        SendError(c, "error", c.Errors.Errors())
        return
    }

    user := map[string]string{
        "username": input.Username,
        "email":    input.Email,
    }
    SendResponse(c, user, "registration success")
}