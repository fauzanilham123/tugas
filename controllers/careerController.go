package controllers

import (
	"net/http"
	"strconv"
	"time"
	"tugas/models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

type careerInput struct {
	Id_category uint   `json:"id_category"`
	Id_position uint   `json:"id_position"`
	Name        string `json:"name"`
	Desc        string `json:"description"`
	Required    string `json:"required"`
	Flag        uint   `json:"flag"`
}



func  GetAllCareer(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var career []models.Career
    sort := c.DefaultQuery("sort", "asc") // Default to ascending if not provided
    sortOrder := "ASC"
    if sort == "desc" {
        sortOrder = "DESC"
    }
	err := db.Where("flag = 1").Order("id "+ sortOrder).Find(&career).Error
    if err != nil{
        SendError(c,"error",c.Errors.Errors())
    }
	SendResponse(c, career, "success")
}

func CreateCareer(c *gin.Context) {
    // Validate input
    var input careerInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    // Create 
    career := models.Career{Id_category: input.Id_category,Id_position: input.Id_position, Name: input.Name, Description: input.Desc, Required: input.Required,Flag: 1,CreatedAt: time.Now()}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&career)

    SendResponse(c, career, "success")
}

func GetCareerById(c *gin.Context) { // Get model if exist
    var career models.Career

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&career).Error; err != nil {
        SendError(c,"Record not found",c.Errors.Errors())
        return
    }

    SendResponse(c, career, "success")
}

func UpdateCareer(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var career models.Career
    if err := db.Where("id = ?", c.Param("id")).First(&career).Error; err != nil {
        SendError(c,"Record not found",c.Errors.Errors())
        return
    }

    // Validate input
    var input careerInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    var updatedInput models.Career
    updatedInput.Id_category = input.Id_category
    updatedInput.Id_position = input.Id_position
    updatedInput.Name = input.Name
    updatedInput.Description = input.Desc
    updatedInput.Required = input.Required
    updatedInput.Flag = input.Flag
    updatedInput.UpdatedAt = time.Now()

    db.Model(&career).Updates(updatedInput)

    SendResponse(c, career, "success")
}

func DeleteCareer(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var career models.Career
    if err := db.Where("id = ?", c.Param("id")).First(&career).Error; err != nil {
        SendError(c, "Record not found", c.Errors.Errors())
        return
    }

    // Set the flag to 0
    if err := db.Model(&career).Update("flag", 0).Error; err != nil {
        SendError(c, "Failed to delete", c.Errors.Errors())
        return
    }

    // Return success response
    SendResponse(c, career, "success")
}

func GetCareerPaginate(c *gin.Context) {
    page := c.DefaultQuery("page", "1")
    perPage := c.DefaultQuery("perPage", "10")

    pageInt, err := strconv.Atoi(page)
    if err != nil || pageInt < 1 {
        pageInt = 1
    }

    perPageInt, err := strconv.Atoi(perPage)
    if err != nil || perPageInt < 1 {
        perPageInt = 10
    }

    db := c.MustGet("db").(*gorm.DB)
    var career []models.Career

    sort := c.DefaultQuery("sort", "asc")
    // Default to ascending if not provided
    sortOrder := "ASC"
    if sort == "desc" {
        sortOrder = "DESC"
    }

    query := db.Where("flag = 1")

    // Get all query parameters and loop through them
    queryParams := c.Request.URL.Query()
     // Remove 'page' and 'perPage' keys from queryParams
    delete(queryParams, "page")
    delete(queryParams, "perPage")
    delete(queryParams, "sort")
    for column, values := range queryParams {
        value := values[0] // In case there are multiple values, we take the first one

        // Apply filtering condition if the value is not empty
        if value != "" {
            query = query.Where(column + " LIKE ?", "%"+value+"%")
        }

    }

    err = query.Offset((pageInt - 1) * perPageInt).Limit(perPageInt).Order("id " + sortOrder).Find(&career).Error
    if err != nil {
        SendError(c, "internal server error", c.Errors.Errors())
        return
    }

    SendResponse(c, career, "success")
    c.JSON(http.StatusOK, gin.H{
        "page":     pageInt,
        "per_page": perPageInt,
    })
}
