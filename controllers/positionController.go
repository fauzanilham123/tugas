package controllers

import (
	"time"
	"tugas/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type positionInput struct {
	Name string `json:"name"`
	Flag uint   `json:"flag"`
}

func GetAllPosition(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var position []models.Position
    sort := c.DefaultQuery("sort", "asc") // Default to ascending if not provided
    sortOrder := "ASC"
    if sort == "desc" {
        sortOrder = "DESC"
    }
	db.Where("flag = 1").Order("id "+sortOrder).Find(&position)
	SendResponse(c, position, "success")
}

func CreatePosition(c *gin.Context) {
    // Validate input
    var input positionInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    // Create 
    position := models.Position{Name: input.Name, Flag: 1, CreatedAt: time.Now()}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&position)

    SendResponse(c, position, "success")
}


func GetPositionById(c *gin.Context) { // Get model if exist
    var position models.Position

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&position).Error; err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    SendResponse(c, position, "success")
}

func UpdatePosition(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var position models.Position
    if err := db.Where("id = ?", c.Param("id")).First(&position).Error; err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    // Validate input
    var input positionInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    var updatedInput models.Position
    updatedInput.Name = input.Name
    updatedInput.Flag = input.Flag
	updatedInput.UpdatedAt = time.Now()
    

    db.Model(&position).Updates(updatedInput)

    SendResponse(c, position, "success")
}

func DeletePosition(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var position models.Position
    if err := db.Where("id = ?", c.Param("id")).First(&position).Error; err != nil {
        SendError(c, "Record not found", c.Errors.Errors())
        return
    }

    // Set the flag to 0
    if err := db.Model(&position).Update("flag", 0).Error; err != nil {
        SendError(c, "Failed to delete", c.Errors.Errors())
        return
    }

    // Return success response
    SendResponse(c, position, "success")
}