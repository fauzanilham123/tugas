package controllers

import (
	"time"
	"tugas/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type categoryInput struct {
	Name string `json:"name"`
	Flag uint   `json:"flag"`
}

func GetAllCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var category []models.Category
    sort := c.DefaultQuery("sort", "asc") // Default to ascending if not provided
    sortOrder := "ASC"
    if sort == "desc" {
        sortOrder = "DESC"
    }
	db.Where("flag = 1").Order("id "+sortOrder).Find(&category)
	SendResponse(c, category, "success")
}

func CreateCategory(c *gin.Context) {
    // Validate input
    var input careerInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    // Create 
    category := models.Category{Name: input.Name, Flag: 1, CreatedAt: time.Now()}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&category)

    SendResponse(c, category, "success")
}

func GetCategoryById(c *gin.Context) { // Get model if exist
    var category models.Category

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        SendError(c,"Record not found",c.Errors.Errors())
        return
    }

    SendResponse(c, category, "success")
}

func UpdateCategory(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var category models.Category
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        SendError(c,"Record not found",c.Errors.Errors())
        return
    }

    // Validate input
    var input categoryInput
    if err := c.ShouldBindJSON(&input); err != nil {
        SendError(c,"error",c.Errors.Errors())
        return
    }

    var updatedInput models.Category
    updatedInput.Name = input.Name
    updatedInput.Flag = input.Flag
	updatedInput.UpdatedAt = time.Now()
    

    db.Model(&category).Updates(updatedInput)

    SendResponse(c, category, "success")
}

func DeleteCategory(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var category models.Category
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        SendError(c, "Record not found", c.Errors.Errors())
        return
    }

    // Set the flag to 0
    if err := db.Model(&category).Update("flag", 0).Error; err != nil {
        SendError(c, "Failed to delete", c.Errors.Errors())
        return
    }

    // Return success response
    SendResponse(c, category, "success")
}
