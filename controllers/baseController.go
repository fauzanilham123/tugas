package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)
 
type BaseController struct {
}
 
type Response struct {
	Success bool        `json:"success"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}
 
func  SendResponse(ctx *gin.Context, result interface{}, message string) {
	response := Response{
		Success: true,
		Code:    200,
		Message: message,
		Result:  result,
	}

	ctx.JSON(http.StatusOK, response)
}

func SendError(ctx *gin.Context, errorMessage string, errorMessages []string) {
	response := Response{
		Success: false,
		Code:   400,
		Message: errorMessage,
		Result:  nil,
	}

	if len(errorMessages) > 0 {
		response.Result = errorMessages
	}

	ctx.JSON(http.StatusBadRequest, response)
}
 
 
// func (c *BaseController) Paginate(items []interface{}, perPage int, page int, options map[string]interface{}) *LengthAwarePaginator {
 
// 	currentPage := resolveCurrentPage()
// 	start := perPage * (currentPage - 1)
// 	end := start + perPage
// 	if end > len(items) {
// 		end = len(items)
// 	}
// 	currentItems := items[start:end]
 
// 	return &LengthAwarePaginator{
// 		Items:        currentItems,
// 		TotalItems:   len(items),
// 		ItemsPerPage: perPage,
// 		CurrentPage:  currentPage,
// 	}
// }