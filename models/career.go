package models

import (
	"time"
)

type (
	// AgeRatingCategory
	Career struct {
		ID          	uint      	`gorm:"primary_key" json:"id"`
		Id_category 	uint    	`json:"id_category"`
		Id_position 	uint		`json:"id_position"`
		Name 			string 		`json:"name"`
		Description 	string 		`json:"description"`
		Required 		string 		`json:"required"`
		Flag			uint 		`json:"flag"`
		CreatedAt  		time.Time 	`json:"created_at"`
		UpdatedAt   	time.Time	`json:"updated_at"`
	}
)