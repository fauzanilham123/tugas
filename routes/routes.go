package routes

import (
	controllers "tugas/controllers"
	"tugas/middlewares"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })

    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)
	r.GET("/career", controllers.GetAllCareer)
    r.GET("/career:id", controllers.GetCareerById)
    r.GET("/career/paginate", controllers.GetCareerPaginate)
	r.GET("/category", controllers.GetAllCategory)
    r.GET("/category:id", controllers.GetCategoryById)
	r.GET("/position", controllers.GetAllPosition)
    r.GET("/position:id", controllers.GetPositionById)
    
	rCareer := r.Group("/career")
    rCareer.Use(middlewares.JwtAuthMiddleware()) //use jwt
    rCareer.POST("/", controllers.CreateCareer)
    rCareer.PATCH("/:id", controllers.UpdateCareer)
    rCareer.DELETE("/:id", controllers.DeleteCareer)
    
    
	rCategory := r.Group("/category")
	rCategory.Use(middlewares.JwtAuthMiddleware())  //use jwt
    rCategory.POST("/", controllers.CreateCategory)
    rCategory.PATCH("/:id", controllers.UpdateCategory)
    rCategory.DELETE("/:id", controllers.DeleteCategory)

	rPosition := r.Group("/position")
	rPosition.Use(middlewares.JwtAuthMiddleware())  //use jwt
    rPosition.POST("/", controllers.CreatePosition)
    rPosition.PATCH("/:id", controllers.UpdatePosition)
    rPosition.DELETE("/:id", controllers.DeletePosition)
    
	return r
}