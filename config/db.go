package config

import (
	"fmt"
	"tugas/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDatabase() *gorm.DB {
    // koneksi untuk docker
    // username := "fauzan"
    // password := "mysecretpassword"
    // host := "tcp(db:3306)"
    // database := "tugas"

    // koneksi untuk local
    username := "root"
    password := ""
    host := "tcp(localhost:3306)"
    database := "tugas"

    dsn := fmt.Sprintf("%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, database)
    db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

    if err != nil {
        panic(err.Error())
    }

	db.AutoMigrate(&models.Career{}, &models.Category{}, &models.Position{}, &models.User{})

    
    return db
}